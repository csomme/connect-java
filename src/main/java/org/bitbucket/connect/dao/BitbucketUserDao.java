package org.bitbucket.connect.dao;

import io.dropwizard.hibernate.AbstractDAO;
import org.bitbucket.connect.objects.database.BitbucketUser;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class BitbucketUserDao extends AbstractDAO<BitbucketUser> {
    public BitbucketUserDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public BitbucketUser findByUuid(String uuid) {
        return this.uniqueResult(this.criteria().add(Restrictions.eq("uuid", uuid)));
    }

}
