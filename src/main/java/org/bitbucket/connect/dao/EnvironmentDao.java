package org.bitbucket.connect.dao;

import io.dropwizard.hibernate.AbstractDAO;
import org.bitbucket.connect.objects.database.Environment;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class EnvironmentDao extends AbstractDAO<Environment> {
    public EnvironmentDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Environment findByUrl(String url) {
        return this.uniqueResult(this.criteria().add(Restrictions.eq("url", url)));
    }

    public Environment findOrCreate(String url) {
        final Environment existing = findByUrl(url);
        if (existing != null)
            return existing;
        final Environment env = new Environment();
        env.setUrl(url);
        return persist(env);
    }
}
