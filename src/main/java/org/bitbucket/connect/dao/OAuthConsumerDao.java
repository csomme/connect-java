package org.bitbucket.connect.dao;

import io.dropwizard.hibernate.AbstractDAO;
import org.bitbucket.connect.objects.database.Environment;
import org.bitbucket.connect.objects.database.OAuthConsumer;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class OAuthConsumerDao extends AbstractDAO<OAuthConsumer> {
    public OAuthConsumerDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public OAuthConsumer findByClientId(Environment env, String clientId) {
        return this.uniqueResult(this.criteria()
                .add(Restrictions.eq("environment", env))
                .add(Restrictions.eq("clientId", clientId)));
    }

    @Override
    public OAuthConsumer persist(OAuthConsumer entity) throws HibernateException {
        return super.persist(entity);
    }
}
