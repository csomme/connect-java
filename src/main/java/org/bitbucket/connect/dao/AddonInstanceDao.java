package org.bitbucket.connect.dao;

import io.dropwizard.hibernate.AbstractDAO;
import org.bitbucket.connect.objects.database.AddonInstance;
import org.bitbucket.connect.objects.database.Environment;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class AddonInstanceDao extends AbstractDAO<AddonInstance> {
    public AddonInstanceDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public AddonInstance findByConnectionKey(String connectionKey) {
        return this.uniqueResult(this.criteria()
                .add(Restrictions.eq("connectionKey", connectionKey)));
    }


    public AddonInstance findByConnectionKey(Environment env, String connectionKey) {
        return this.uniqueResult(this.criteria()
                .add(Restrictions.eq("connectionKey", connectionKey))
                .add(Restrictions.eq("environment", env)));
    }

    @Override
    public AddonInstance persist(AddonInstance entity) throws HibernateException {
        return super.persist(entity);
    }

    public void delete(AddonInstance instance) {
        this.currentSession().delete(instance);
    }
}
