package org.bitbucket.connect.objects.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="addon_instance")
public class AddonInstance {
    Long id;
    Environment environment;
    OAuthConsumer consumer;
    String connectionKey;
    String accountUuid;
    String sharedSecret;

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(optional=false)
    public Environment getEnvironment() {
        return environment;
    }
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @ManyToOne
    public OAuthConsumer getConsumer() {
        return consumer;
    }
    public void setConsumer(OAuthConsumer consumer) {
        this.consumer = consumer;
    }

    @Column(name="connection_key", nullable=false)
    public String getConnectionKey() {
        return connectionKey;
    }
    public void setConnectionKey(String connectionKey) {
        this.connectionKey = connectionKey;
    }

    @Column(name="account_uuid", nullable=false)
    public String getAccountUuid() {
        return accountUuid;
    }
    public void setAccountUuid(String accountUuid) {
        this.accountUuid = accountUuid;
    }

    @Column(name="shared_secret", nullable=false)
    public String getSharedSecret() {
        return sharedSecret;
    }
    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }
}
