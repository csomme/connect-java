package org.bitbucket.connect.objects.lifecycle;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Callback {
    public enum EventType {
        installed, uninstalled, enabled, diabled;
    }

    public String key;
    public String clientKey;
    public String publicKey;
    public String sharedSecret;
    public String serverVersion;
    public String baseUrl;
    public String productType;
    public EventType eventType;
    public OAuthConsumer consumer;
    public Principal principal;
}
