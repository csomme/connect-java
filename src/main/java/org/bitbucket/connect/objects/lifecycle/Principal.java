package org.bitbucket.connect.objects.lifecycle;

public class Principal {
    public String displayName;
    public String username;
    public String website;
    public String location;
    public String type;
    public String uuid;
}
