package org.bitbucket.connect.objects.lifecycle;

public class OAuthConsumer {
    public String name;
    public String description;
    public String url;
    public String key;
    public String secret;
}
