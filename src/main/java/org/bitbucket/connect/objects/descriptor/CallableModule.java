package org.bitbucket.connect.objects.descriptor;

import java.util.Map;

public abstract class CallableModule {
    public String url;
    public Map<String, String> params;
}
