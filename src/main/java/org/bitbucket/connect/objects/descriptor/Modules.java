package org.bitbucket.connect.objects.descriptor;

import java.util.List;

public class Modules {
    public List<Webhook> webhooks;
    public List<ExtensionPointModule> adminPages;
    public List<ExtensionPointModule> configurePage;
    public List<ExtensionPointModule> generalPages;
    public List<ExtensionPointModule> repoPages;
    public List<ExtensionPointModule> webPanels;
    public List<WebItem> webItems;
    public List<PageModule> profileTabs;
    public OAuthConsumer oauthConsumer;
}
