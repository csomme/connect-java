package org.bitbucket.connect.objects.descriptor;

import java.util.List;

public class PageModule extends CallableModule {
    public String key;
    public I18Property name = new I18Property();
    public List<SimpleCondition> conditions;
}
