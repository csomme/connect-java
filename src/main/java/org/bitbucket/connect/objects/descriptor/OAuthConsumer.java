package org.bitbucket.connect.objects.descriptor;

import java.util.ArrayList;
import java.util.List;

public class OAuthConsumer {
    public String clientId;
    public List<String> scopes = new ArrayList<>();
}
