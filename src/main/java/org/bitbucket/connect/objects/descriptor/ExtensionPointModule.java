package org.bitbucket.connect.objects.descriptor;

public class ExtensionPointModule extends PageModule {
    public Integer weight;
    public String tooltip;
    public String location;
    public Icon icon;
}
