package org.bitbucket.connect.objects.descriptor;

public class BitbucketConnection {
    public String secret;
    public String baseUrl;
    public String accountUuid;
}
