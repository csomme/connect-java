package org.bitbucket.connect.objects.descriptor;

import java.util.HashSet;
import java.util.Set;

public class Descriptor {
    public enum Scope {
        repository,
        repository_write,
        repository_admin,
        snippet,
        snippet_write,
        account,
        email,
        account_write,
        team,
        team_write,
        webhook,
        wiki,
        issue,
        issue_write,
        pullrequest,
        pullrequest_write,
    }

    public String name;
    public String description;
    public Lifecycle lifecycle = new Lifecycle();
    public Authentication authentication = new Authentication(Authentication.AuthType.jwt);
    public String baseUrl;
    public String key;
    public Vendor vendor = new Vendor();
    public Modules modules = new Modules();
    public Set<Scope> scopes = new HashSet<Scope>();
}
