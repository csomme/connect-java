package org.bitbucket.connect.objects.descriptor;

public class Icon {
    public String url;
    public Integer height;
    public Integer width;
}
