package org.bitbucket.connect.objects.descriptor;

public class Lifecycle {
    public String installed;
    public String uninstalled;
    public String enabled;
    public String disabled;
}
