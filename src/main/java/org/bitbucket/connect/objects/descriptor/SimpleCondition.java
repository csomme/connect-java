package org.bitbucket.connect.objects.descriptor;

import java.util.HashMap;
import java.util.Map;

public class SimpleCondition {
    public boolean invert;
    public Map<String, String> params = new HashMap<>();
    public String condition;
}
