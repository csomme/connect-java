package org.bitbucket.connect.objects.descriptor;

public class Authentication {
    public enum AuthType {
        jwt,
        none
    }

    public Authentication(AuthType type) {
        this.type = type;
    }

    public AuthType type;
}
