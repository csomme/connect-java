package org.bitbucket.connect.objects.descriptor;

public class CompositeCondition {
    public String type;
    public SimpleCondition condition;
    public CompositeCondition conditions;
}
