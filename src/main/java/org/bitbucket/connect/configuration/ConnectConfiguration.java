package org.bitbucket.connect.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import org.bitbucket.connect.objects.descriptor.Descriptor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ConnectConfiguration extends Configuration {
    public String name;
    public String description;
    public String key;
    public String baseUrl;
    public String vendorUrl;
    public String vendorName;
    public String clientId;


    private Descriptor.Scope[] scopes;

    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }
}
