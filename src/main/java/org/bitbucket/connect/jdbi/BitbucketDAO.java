package org.bitbucket.connect.jdbi;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface BitbucketDAO {
    @SqlUpdate("")
    void createConnection();
}
