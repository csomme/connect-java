package org.bitbucket.connect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Modules {
    public enum Location {
        account_admin("org.bitbucket.account.admin"),
        branch_summary_actions("org.bitbucket.branch.summary.actions"),
        branch_summary_info("org.bitbucket.branch.summary.info"),
        commit_summary_actions("org.bitbucket.commit.summary.actions"),
        commit_summary_info("org.bitbucket.commit.summary.info"),
        pullrequest_summary_actions("org.bitbucket.pullrequest.summary.actions"),
        pullrequest_summary_info("org.bitbucket.pullrequest.summary.info"),
        repository_actions("org.bitbucket.repository.actions"),
        repository_admin("org.bitbucket.repository.admin"),
        repository_navigation("org.bitbucket.repository.navigation"),
        repository_overview_info("org.bitbucket.repository.overview.informationPanel");

        public final String name;

        Location(String name) {
            this.name = name;
        }
    }

    public enum ExtensionPointModuleType {
        ADMIN_PAGE("adminPages"),
        CONFIGURE_PAGE("configurePage"),
        GENERAL_PAGE("generalPages"),
        REPO_PAGE("repoPages"),
        WEB_PANEL("webPanels");

        public final String name;

        ExtensionPointModuleType(String name) {
            this.name = name;
        }
    }

    public enum PageModuleType {
        PROFILE_TAB("profileTabs");

        public final String name;

        PageModuleType(String name) {
            this.name = name;
        }

    }

    public @interface Param {
        String name();
        String value();
    }

    public @interface Condition {
        public enum Name {

        }

        Name name();
        boolean invert() default false;
        Param[] params() default {};
    }

    public @interface CompositeCondition {
        public enum Operator {
            and, or
        }

        Operator operator() default Operator.and;
        Condition[] value();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @Module
    public @interface ExtensionPointModule {
        ExtensionPointModuleType type();
        String key();
        String name();
        int weight() default -1;
        String tooltip() default "";
        Location location();
        Param[] params() default {};
        Condition[] conditions() default {};
    }


    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @Module public @interface WebItem {}

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @Module public @interface PageModule {
        PageModuleType type();
        String key();
        String name();
        Param[] params() default {};
        Condition[] conditions() default{};
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.ANNOTATION_TYPE)
    public static @interface Module {}
}
