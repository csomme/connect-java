package org.bitbucket.connect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Lifecycle {
    public enum Type {
        DISABLED,
        ENABLED,
        INSTALLED,
        UNINSTALLED;

        public final String name;
        Type() {
            name = this.name().toLowerCase();
        }
    }

    Type value();
}
