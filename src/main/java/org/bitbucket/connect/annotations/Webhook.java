package org.bitbucket.connect.annotations;

@Modules.Module
public @interface Webhook {

    public enum Hook {
        all("*"),
        issue_comment_created("issue:comment_created"),
        issue_created("issue:created"),
        repo_push("repo:push"),
        issue_updated("issue:updated"),
        pullrequest_approved("pullrequest:approved"),
        pullrequest_comment_created("pullrequest:comment_created"),
        pullrequest_comment_deleted("pullrequest:comment_deleted"),
        pullrequest_created("pullrequest:created"),
        pullrequset_fulfilled("pullrequest:fulfilled"),
        pullrequest_rejected("pullrequest:rejected"),
        pullrequest_superseded("pullrequest:superseded"),
        pullrequest_unapproved("pullrequest_unapproved"),
        pullrequest_updated("pullrequest:updated"),
        repo_branch_created("repo:branch_created"),
        repo_branch_deleted("repo:branch_deleted"),
        repo_commit("repo:commit"),
        repo_commit_comment_created("repo:commit_comment_created"),
        repo_fork("repo:fork"),
        user_account_property_changed("user:account_property_changed");

        public final String name;

        Hook(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    Hook[] value();
}
