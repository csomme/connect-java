package org.bitbucket.connect.filters;

import org.bitbucket.connect.annotations.Lifecycle;
import org.bitbucket.connect.annotations.Modules;
import org.bitbucket.connect.annotations.VerificationExempt;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Provider
public class Binder implements DynamicFeature {
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        final Method method = resourceInfo.getResourceMethod();
        if (method.getAnnotation(VerificationExempt.class) != null)
            return;

        for (final Annotation methodAnnotation : method.getAnnotations()) {
            if ((methodAnnotation.annotationType().equals(Lifecycle.class) &&
                    ((Lifecycle)methodAnnotation).value() != Lifecycle.Type.INSTALLED) ||
                    methodAnnotation.annotationType().getAnnotation(Modules.Module.class) != null) {
                context.register(JWTVerificationFilter.class);
                return;
            }
        }

        context.register(SessionVerificationFilter.class);
    }
}
