package org.bitbucket.connect.filters;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import io.dropwizard.hibernate.UnitOfWork;
import org.bitbucket.connect.dao.AddonInstanceDao;
import org.bitbucket.connect.objects.database.AddonInstance;
import org.glassfish.hk2.api.Factory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.text.ParseException;

public class JWTVerificationFilter implements ContainerRequestFilter, Factory<AddonInstance> {
    @Context AddonInstanceDao addonDao;
    @Context SessionFactory factory;

    private final static ThreadLocal<AddonInstance> addons = new ThreadLocal<>();

    @Override
    @UnitOfWork
    public void filter(ContainerRequestContext requestContext) throws IOException {
        addons.set(null);
        final String authorization = requestContext.getHeaderString("Authorization");
        if (authorization == null)
            ;

        if (!authorization.startsWith("JWT"))
            ;

        final Session session = factory.openSession();
        try {
            ManagedSessionContext.bind(session);
            final JWT jwt = JWTParser.parse(authorization.substring(4).trim());
            if (!(jwt instanceof SignedJWT))
                ;
            final AddonInstance addon = addonDao.findByConnectionKey(
                    jwt.getJWTClaimsSet().getIssuer());
            if (!((SignedJWT) jwt).verify(new MACVerifier(addon.getSharedSecret())))
                ;
            // set thread local
            addons.set(addon);
        } catch (ParseException | JOSEException e) {
            e.printStackTrace();
        } finally {
            session.close();
            ManagedSessionContext.unbind(factory);
        }

    }

    @Override
    public AddonInstance provide() {
        return addons.get();
    }

    @Override
    public void dispose(AddonInstance instance) {}
}
