package org.bitbucket.connect.api;

import io.dropwizard.hibernate.UnitOfWork;
import org.bitbucket.connect.annotations.Lifecycle;
import org.bitbucket.connect.annotations.VerificationExempt;
import org.bitbucket.connect.dao.AddonInstanceDao;
import org.bitbucket.connect.dao.EnvironmentDao;
import org.bitbucket.connect.dao.OAuthConsumerDao;
import org.bitbucket.connect.objects.database.AddonInstance;
import org.bitbucket.connect.objects.database.Environment;
import org.bitbucket.connect.objects.database.OAuthConsumer;
import org.bitbucket.connect.objects.lifecycle.Callback;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("lifecycle")
public class LifecycleResource {
    private final EnvironmentDao environmentDao;
    private final AddonInstanceDao addonDao;
    private final OAuthConsumerDao oauthDao;

    public LifecycleResource(@Context EnvironmentDao environmentDao,
                             @Context AddonInstanceDao addonDao,
                             @Context OAuthConsumerDao oauthDao) {

        this.environmentDao = environmentDao;
        this.addonDao = addonDao;
        this.oauthDao = oauthDao;
    }

    @POST
    @Path("installed")
    @Lifecycle(Lifecycle.Type.INSTALLED)
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public void installed(Callback callback) {
        final Environment environment = environmentDao.findOrCreate(callback.baseUrl);
        AddonInstance addon = addonDao.findByConnectionKey(environment, callback.clientKey);
        // normal case -- it really should always be null
        if (addon == null)
            addon = new AddonInstance();
        addon.setEnvironment(environment);
        addon.setConnectionKey(callback.clientKey);
        addon.setSharedSecret(callback.sharedSecret);
        if (callback.consumer != null) {
            OAuthConsumer consumer = oauthDao.findByClientId(environment, callback.consumer.key);
            if (consumer == null) {
                consumer = new OAuthConsumer();
                consumer.setEnvironment(environment);
                consumer.setClientId(callback.consumer.key);
                consumer.setClientSecret(callback.consumer.secret);
                consumer = oauthDao.persist(consumer);
            }
            addon.setConsumer(consumer);
        }
        addonDao.persist(addon);
    }

    @POST
    @Path("uninstalled")
    @Lifecycle(Lifecycle.Type.UNINSTALLED)
    @UnitOfWork
    public void uninstalled(@Context AddonInstance instance, Callback callback) {
        addonDao.delete(instance);
    }
}
