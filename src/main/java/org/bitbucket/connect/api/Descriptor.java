package org.bitbucket.connect.api;

import org.bitbucket.connect.annotations.*;
import org.bitbucket.connect.annotations.Lifecycle;
import org.bitbucket.connect.configuration.ConnectConfiguration;
import org.bitbucket.connect.dao.AddonInstanceDao;
import org.bitbucket.connect.dao.EnvironmentDao;
import org.bitbucket.connect.objects.descriptor.CallableModule;
import org.bitbucket.connect.objects.descriptor.ExtensionPointModule;
import org.bitbucket.connect.objects.descriptor.OAuthConsumer;
import org.bitbucket.connect.objects.descriptor.PageModule;

import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Path("/descriptor")
public class Descriptor {
    private final EnvironmentDao environmentDao;
    private final AddonInstanceDao addonDao;
    private final OAuthConsumer oauthDao;
    private final org.bitbucket.connect.objects.descriptor.Descriptor descriptor;

    public Descriptor(@Context Application application,
                      @Context ConnectConfiguration configuration,
                      @Context EnvironmentDao environmentDao,
                      @Context AddonInstanceDao addonDao,
                      @Context OAuthConsumer oauthDao) {
        this.environmentDao = environmentDao;
        this.addonDao = addonDao;
        this.oauthDao = oauthDao;

        this.descriptor = new org.bitbucket.connect.objects.descriptor.Descriptor();

        this.descriptor.name = configuration.name;
        this.descriptor.description = configuration.description;
        this.descriptor.key = configuration.key;
        this.descriptor.baseUrl = configuration.baseUrl;
        this.descriptor.modules.oauthConsumer = new OAuthConsumer();
        this.descriptor.modules.oauthConsumer.clientId = configuration.clientId;
        this.descriptor.vendor.name = configuration.vendorName;
        this.descriptor.vendor.url = configuration.vendorUrl;

        final Set<Class<?>> allResourceClasses = new HashSet<>();
        final Set<Class<?>> classes = application.getClasses();
        if (classes != null)
            allResourceClasses.addAll(classes);

        final Set<Object> singletons = application.getSingletons();
        if (singletons != null)
            for (Object obj : singletons)
                allResourceClasses.add(obj.getClass());
        findModules(allResourceClasses);
    }

    private String getUrl(Method method) {
        final Path classPath =
                method.getDeclaringClass().getAnnotation(Path.class);
        final Path methodPath = method.getAnnotation(Path.class);

        if (classPath == null && methodPath == null)
            return "";
        else if (methodPath == null)
            return classPath.value();
        else if (classPath == null)
            return methodPath.value();

        if (classPath.value().endsWith("/") && methodPath.value().startsWith("/"))
            return classPath.value() + methodPath.value().substring(1);
        else if (!classPath.value().endsWith("/") && ! methodPath.value().startsWith("/"))
            return classPath.value() + "/" + methodPath.value();
        else
            return classPath.value() + methodPath.value();
    }

    private void createModule(String prefix, Method method, Annotation moduleInfo) {
        String url = prefix + getUrl(method);
        if (moduleInfo instanceof Webhook) {
            addWebhooks((Webhook) moduleInfo, url);
        } else if (moduleInfo instanceof Modules.ExtensionPointModule) {

            final Modules.ExtensionPointModule epm = (Modules.ExtensionPointModule) moduleInfo;

            try {
                final Field field = this.descriptor.modules.getClass().getField(epm.type().name);
                List<ExtensionPointModule> modules = (List<ExtensionPointModule>) field.get(this.descriptor.modules);
                if (modules == null) {
                    modules = new ArrayList<>();
                    field.set(this.descriptor.modules, modules);
                }

                modules.add(initExtensionPointModule(
                        new ExtensionPointModule(), url, epm.key(), epm.name(),
                        epm.weight(), epm.tooltip(), epm.location(),
                        epm.params()));
            } catch (IllegalAccessException | NoSuchFieldException e) { }


        } else if (moduleInfo instanceof Modules.PageModule) {
            try {
                final Modules.PageModule pm = (Modules.PageModule) moduleInfo;
                final Field field = this.descriptor.modules.getClass().getField(pm.type().name);
                List<PageModule> modules = (List<PageModule>) field.get(this.descriptor.modules);
                if (modules == null) {
                    modules = new ArrayList<>();
                    field.set(this.descriptor.modules, modules);
                }

                modules.add(initPageModule(new PageModule(), url, pm.key(), pm.name(), pm.params()));
            } catch (IllegalAccessException | NoSuchFieldException e) { }
        } else if (moduleInfo instanceof Modules.WebItem) {
        }
    }

    private void createLifecyleMethod(String prefix, Method method) {
        final String url = prefix + getUrl(method);
        final Lifecycle lifecycle = method.getAnnotation(Lifecycle.class);
        if (lifecycle == null)
            return;

        try {
            final Field field = this.descriptor.lifecycle.getClass().getField(lifecycle.value().name);
            field.set(this.descriptor.lifecycle, url);
        } catch (NoSuchFieldException | IllegalAccessException e) {}
    }

    private void addWebhooks(Webhook moduleInfo, String url) {
        for (Webhook.Hook hook : moduleInfo.value()) {
            final org.bitbucket.connect.objects.descriptor.Webhook webhook =
                    new org.bitbucket.connect.objects.descriptor.Webhook();
            webhook.url = url;
            webhook.event = hook.name;
            if (descriptor.modules.webhooks == null)
                descriptor.modules.webhooks = new ArrayList<>();
            descriptor.modules.webhooks.add(webhook);
        }
    }

    private PageModule initPageModule(PageModule pageModule, String url, String key, String name, Modules.Param... params) {
        pageModule.url = url;
        pageModule.key = key;
        pageModule.name.value = name;
        if (params != null && params.length > 0) {
            pageModule.params = new HashMap<>();
            for (final Modules.Param param : params)
                pageModule.params.put(param.name(), param.value());
        }
        return pageModule;
    }

    private ExtensionPointModule initExtensionPointModule(ExtensionPointModule module, String url, String key, String name,
                                          int weight, String tooltip, Modules.Location location, Modules.Param... params) {
        initPageModule(module, url, key, name, params);
        if (weight > 0) module.weight = weight;
        if (tooltip.length() > 0) module.tooltip = tooltip;
        module.location = location.name;
        return module;
    }

    private void findModules(Set<Class<?>> resourceClasses) {
        final Set<CallableModule> modules = new HashSet<CallableModule>();

        for (Class<?> cls : resourceClasses)
            findModulesForClass("", cls);
    }

    private void findModulesForClass(String prefix, Class<?> cls) {
        final Path path = cls.getAnnotation(Path.class);
        if (path == null)
            return;

        METHOD: for (final Method method : cls.getMethods()) {
            final boolean hasPath = method.getAnnotation(Path.class) != null;
            boolean hasMethod = false;
            if (method.getAnnotation(Lifecycle.class) != null) {
                createLifecyleMethod(prefix, method);
                continue;
            }
            for (final Annotation annotation : method.getAnnotations()) {
                final Class<? extends Annotation> annotationType = annotation.annotationType();
                if (annotationType.getAnnotation(Modules.Module.class) != null) {
                    createModule(prefix, method, annotation);
                    continue METHOD;
                }
                hasMethod = hasMethod || annotationType.getAnnotation(HttpMethod.class) != null;
            }
            if (hasPath && !hasMethod)
                findModulesForClass(getUrl(method), method.getReturnType());
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @VerificationExempt
    public org.bitbucket.connect.objects.descriptor.Descriptor getDescriptor() {
        return this.descriptor;
    }
}
