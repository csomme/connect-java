package org.bitbucket.connect.api;

import org.bitbucket.connect.annotations.Modules;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/stuff")
public class Foo {
    @GET
    @Path("/{bar}")
    @Modules.ExtensionPointModule(
            type=Modules.ExtensionPointModuleType.REPO_PAGE,
            location=Modules.Location.repository_navigation,
            name="Configure", key="asdf")
    public String baz(@PathParam("bar") String bar) {
        return "baz";
    }
}
