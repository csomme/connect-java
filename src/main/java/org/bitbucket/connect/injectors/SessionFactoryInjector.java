package org.bitbucket.connect.injectors;

import org.glassfish.hk2.api.Factory;
import org.hibernate.SessionFactory;

public class SessionFactoryInjector implements Factory<SessionFactory> {
    private final SessionFactory factory;

    public SessionFactoryInjector(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public SessionFactory provide() {
        return this.factory;
    }

    @Override
    public void dispose(SessionFactory factory) {}
}
