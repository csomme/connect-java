package org.bitbucket.connect.injectors;

import org.bitbucket.connect.dao.EnvironmentDao;
import org.glassfish.hk2.api.Factory;
import org.hibernate.SessionFactory;

import javax.ws.rs.core.Context;

public class EnvironmentDaoInjector implements Factory<EnvironmentDao> {
    private SessionFactory sessionFactory;

    public EnvironmentDaoInjector(@Context SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public EnvironmentDao provide() {
        return new EnvironmentDao(this.sessionFactory);
    }

    @Override
    public void dispose(EnvironmentDao dao) {}
}
