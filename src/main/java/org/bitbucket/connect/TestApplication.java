package org.bitbucket.connect;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import io.dropwizard.Application;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bitbucket.connect.api.Descriptor;
import org.bitbucket.connect.api.Foo;
import org.bitbucket.connect.api.LifecycleResource;
import org.bitbucket.connect.configuration.ConnectConfiguration;
import org.bitbucket.connect.dao.AddonInstanceDao;
import org.bitbucket.connect.dao.BitbucketUserDao;
import org.bitbucket.connect.dao.EnvironmentDao;
import org.bitbucket.connect.dao.OAuthConsumerDao;
import org.bitbucket.connect.filters.Binder;
import org.bitbucket.connect.filters.JWTVerificationFilter;
import org.bitbucket.connect.objects.database.AddonInstance;
import org.bitbucket.connect.objects.database.BitbucketUser;
import org.bitbucket.connect.objects.database.OAuthConsumer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ServerProperties;
import org.hibernate.SessionFactory;

import java.util.HashMap;
import java.util.Map;

public class TestApplication extends Application<ConnectConfiguration> {

    private final HibernateBundle<ConnectConfiguration> hibernate =
            new HibernateBundle<ConnectConfiguration>(
                    org.bitbucket.connect.objects.database.Environment.class,
                    OAuthConsumer.class,
                    AddonInstance.class,
                    BitbucketUser.class) {
                @Override
                public PooledDataSourceFactory getDataSourceFactory(ConnectConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
    };

    private final MigrationsBundle<ConnectConfiguration> migrations =
            new MigrationsBundle<ConnectConfiguration>() {
                @Override
                public PooledDataSourceFactory getDataSourceFactory(ConnectConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    public static void main(String[] args) throws Exception {
        new TestApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ConnectConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(migrations);
    }

    @Override
    public void run(final ConnectConfiguration connectConfiguration, Environment environment) throws Exception {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(ServerProperties.WADL_FEATURE_DISABLE, false);
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        environment.getObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        environment.jersey().getResourceConfig().addProperties(properties);
        environment.jersey().register(Binder.class);
        environment.jersey().register(Descriptor.class);
        environment.jersey().register(Foo.class);
        environment.jersey().register(LifecycleResource.class);
        environment.jersey().register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(connectConfiguration).to(ConnectConfiguration.class);
                bind(hibernate.getSessionFactory()).to(SessionFactory.class);
                bind(new EnvironmentDao(hibernate.getSessionFactory())).to(EnvironmentDao.class);
                bind(new AddonInstanceDao(hibernate.getSessionFactory())).to(AddonInstanceDao.class);
                bind(new BitbucketUserDao(hibernate.getSessionFactory())).to(BitbucketUserDao.class);
                bind(new OAuthConsumerDao(hibernate.getSessionFactory())).to(OAuthConsumerDao.class);
                bindFactory(JWTVerificationFilter.class).to(AddonInstance.class);
            }
        });
    }
}
